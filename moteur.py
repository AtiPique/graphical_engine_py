import os
from lib_math import *

if os.path.exists("logs.txt"):
  os.remove("logs.txt")
f = open("logs.txt", "w")

s = open("type.txt", "w")

width,height = os.get_terminal_size()
height -= 1
pixelBuffer = ['*']*(width*height)

def draw():
    print(''.join(pixelBuffer),end='')

def clear(char):
    for i in range(width*height):
        pixelBuffer[i] = char

def putPixel(v, char):
    px = round(v.x)
    py = round(v.y)
    if 0 <= px <width and 0 <= py < height:
        pixelBuffer[py * width + px] = char

def putTriangle(tri, char):
    def eq(p, a, b):
        return(a.x-p.x)*(b.y-p.y)-(a.y-p.y)*(b.x-p.x)


    xmin = round(min(tri.p1.x, tri.p2.x, tri.p3.x))
    xmax = round(max(tri.p1.x, tri.p2.x, tri.p3.x))+1
    ymin = round(min(tri.p1.y, tri.p2.y, tri.p3.y))
    ymax = round(max(tri.p1.y, tri.p2.y, tri.p3.y))+1

    for y in range(ymin, ymax):
        if 0 <= y < height:
            for x in range(xmin, xmax):
                pos = Vec2(x,y)
                w1 = eq(pos, tri.p3, tri.p1)
                w2 = eq(pos, tri.p1, tri.p2)
                w3 = eq(pos, tri.p2, tri.p3)
                if (w1 >= 0 and w2 >= 0 and w3 >= 0) or (-w1 >= 0 and -w2 >= 0 and -w3 >= 0):
                    putPixel(pos,char)

def putSquare(sqa, char):
        def eq(p, a, b):
            return(a.x-p.x)*(b.y-p.y)-(a.y-p.y)*(b.x-p.x)


        xmin = round(min(sqa.p1.x, sqa.p2.x, sqa.p3.x))
        xmax = round(max(sqa.p1.x, sqa.p2.x, sqa.p3.x))+1
        ymin = round(min(sqa.p1.y, sqa.p2.y, sqa.p3.y))
        ymax = round(max(sqa.p1.y, sqa.p2.y, sqa.p3.y))+1

        for y in range(ymin, ymax):
            if 0 <= y < height:
                for x in range(xmin, xmax):
                    pos = Vec2(x,y)
                    w1 = eq(pos, sqa.p1, sqa.p2)
                    w2 = eq(pos, sqa.p2, sqa.p3)
                    w3 = eq(pos, sqa.p3, sqa.p4)
                    w4 = eq(pos, sqa.p4, sqa.p1)
                    if (w1 >= 0 and w2 >= 0 and w3 >= 0 and w4 >= 0) or (-w1 >= 0 and -w2 >= 0 and -w3 >= 0 and -w4 >= 0):
                        putPixel(pos,char)

def autoSquare(x, n, char): # x pos in term, n square side size
    nPrim = (x + n) - 1
    y = 0 + n
    print(y)
    clear(' ')
    
    sqa = Square(Vec2(x, x-1),
                 Vec2(nPrim, x-1),
                 Vec2(nPrim, y),
                 Vec2(x, y))

    putSquare(sqa, char)

    draw()
    print(y)

''' // Moving triangle 

w=width
a=0.1
t=a 

while t != 0:
    s.write(f"{pixelBuffer}")
    t = t +a 
    if round(t) == w:
        a = -0.1    

    if round(t) == 0:
        a = 0.1

    f.write(f"width : {w}\t t: {t}\t t (rounded): {round(t)} TRUE\n")
    clear(' ')
    tri = Triangle(Vec2(10,10),
                   Vec2(80,15),
                   Vec2(t,30))
    putTriangle(tri, '7')



    draw()

s.write(f"{pixelBuffer}")
'''


'''
w=width
a=0.1
t=21


while t != 20:
    
    t = t + a 
    if round(t) == w:
        a = -0.1

    if round(t) == 20:
        a = 0.1


clear(' ')

sqa = Square(Vec2(10, 10),
                 Vec2(t, 10),
                 Vec2(t, 10),
                 Vec2(10, 10))

putSquare(sqa, '@')

draw()
'''

autoSquare(10, 5, 'o')
