class Vec2:
    def __init__(self, x, y) -> None:
        self.x = x
        self.y = y

class Triangle:
    def __init__(self, p1, p2, p3) -> None:
        self.p1 = p1
        self.p2 = p2
        self.p3 = p3
        
class Square:
    def __init__(self, p1, p2, p3, p4) -> None:
        self.p1 = p1
        self.p2 = p2
        self.p3 = p3
        self.p4 = p4